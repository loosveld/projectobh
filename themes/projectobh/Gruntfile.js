module.exports = function(grunt) {

  // configure tasks
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    compass: {
      dist: {
        options: {
          sassDir: 'sass',
          cssDir: 'css',
          //httpPath: '/sites/projectobh.dev/themes/projectobh',
          imagesDir: 'images',
          javascriptsDir: 'js',
          importPath: '/Users/bram/Dropbox/Projects/_base/sites/all/libraries',
          fontsDir: 'css/fonts'
        }
      }
    },

    imagemin: {
      dist: {
        options: {
          optimizationLevel: 7,
          progressive: true
        },
        files: [{
          expand: true,
          cwd: 'images/',
          src: '**/*',
          dest: 'images/'
        }]
      }
    },

    watch: {
      sass: {
        files: ['sass/*.scss'],
        tasks: ['compass:dist']
      },
      css: {
        files: ['*.css']
      },
      livereload: {
        files: ['*.html', '*.php', 'images/**/*.{png,jpg,jpeg,gif,webp,svg}'],
        options: { livereload: true }
      }
    }

  });

  // load plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');

  // register tasks
  grunt.registerTask('default',['watch','compass']);
};