<?php
	global $base_url; 

	if(drupal_is_front_page()) {
		unset($page['content']['system_main']['default_message']);
	}

	if($node->type == 'webform' && $user->uid == 0):
		drupal_goto('user');
	endif;

?>

<nav class="top-bar" id="header" data-topbar>
	<a href="/" id="logo"></a>
	<?php print render($page['header']); ?>		
</nav>

<div id="container">
	<section id="main" class="clearfix">

		<?php if($page['sidebar']): ?>

		<div id="sidebar">
			<div class="block module_title <?php echo arg(0); ?> module_nid_<?php print $module_nid; ?>" data-url="<?php echo url('node/'.$module_nid);?>">
				<h2><?php print $module_title; ?></h2>
			</div>
			<?php print render($page['sidebar']); ?>
		</div>
		
		<?php endif; ?>

		<div id="content" >
		
			<div class="content-system">
				
				<div id="print"></div>

				<?php if ($messages): ?>
					<div id="messages">
						<?php print $messages; ?>
					</div>
				<?php endif; ?>

				<?php if ($breadcrumb): ?>
					<div id="breadcrumb">
						<?php print $breadcrumb; ?>
					</div>
				<?php endif; ?>
				
				<?php if (!empty($tabs['#primary'])): ?><?php print render($primary_local_tasks); ?><?php print render($tabs); ?><?php endif; ?>


				<?php if ($title_prefix || $title || $title_suffix): ?>
				<div id="title">
					<?php print render($title_prefix); ?>
					<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
					<?php print render($title_suffix); ?>
				</div>
				<?php endif; ?>
							
				<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
				
			</div>
	
			<?php if ($page['content']): ?>
				<?php print render($page['content']); ?>
			<?php endif; ?>
			
		</div>
				  
	</section>
	<div class="push"></div>			
</div>


<footer id="footer" >
	<?php print render($page['footer']); ?>
</footer>

