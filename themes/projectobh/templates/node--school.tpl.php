<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> data-url="<?php print $node_url; ?>">

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <h2>Gemeente</h2>
    <?php if($content['field_school_city']): ?>
        <?php print render($content['field_school_city']); ?>
    <?php endif; ?>
    <h2>Schoolnet</h2>
    <?php if($content['field_school_net']): ?>
        <?php print render($content['field_school_net']); ?>
    <?php endif; ?>
  </div>
  
</div>