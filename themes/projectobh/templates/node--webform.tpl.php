<?php

  global $user;
  $user = user_load($user->uid);
  $school = $user->field_user_school['und'][0]['nid'];
  $allowed = array('begeleider', 'administrator');

?>

<div id="node-<?php print $node->nid; ?>" ng-app="obh_exercise" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> data-url="<?php print $node_url; ?>">

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h3<?php print $title_attributes; ?>><?php print $title; ?></h3>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if($view_mode == 'full'): ?>

    <dl class="sub-nav" ng-controller="NavCtrl">

      <?php if (count(array_intersect($user->roles, $allowed)) > 0): ?>
        <dd ng-class="{active: $route.current.loadedTemplateUrl == 'content.html'}"><a href="#/inhoud" >Inhoud & Materiaal</a></dd>
      <?php endif; ?>

      <?php if($content['webform'] or $node->nid == 96 or $node->nid == 97): ?>
        <dd ng-class="{active: $route.current.loadedTemplateUrl == 'exercise.html'}" id="exercise"><a href="#/oefening">Digitale Oefening</a></dd>
      <?php endif; ?>

      <?php if($node->nid == 143 && count(array_intersect($user->roles, $allowed)) > 0): ?>
        <dd ng-class="{active: $route.current.loadedTemplateUrl == 'database.html'}" id="exercise"><a href="#/databank">Databank</a></dd>
      <?php endif; ?>
      
      <?php if($content['field_webform_methods'] && trim(render($content['field_webform_type'])) == 'exercise' && count(array_intersect($user->roles, $allowed)) > 0): ?>
        <dd ng-class="{active: $route.current.loadedTemplateUrl == 'method.html'}"><a href="#/methodieken">Alternatieve Methodieken</a></dd>
      <?php endif; ?>
      
      <?php if ($content['field_webform_tips_tricks'] && count(array_intersect($user->roles, $allowed)) > 0): ?>
      <dd ng-class="{active: $route.current.loadedTemplateUrl == 'tipstricks.html'}"><a href="#/tipstricks">Tips & Tricks</a></dd>
      <?php endif; ?>
      
      <?php if($content['webform'] && $node->nid != 12 && $node->nid != 96): ?>
      <dd ng-class="{active: $route.current.loadedTemplateUrl == 'result.html'}"><a href="#/resultaten">Resultaten</a></dd>
      <?php endif; ?>
      
      <?php if (count(array_intersect($user->roles, $allowed)) > 0): ?>
      <dd ng-class="{active: $route.current.loadedTemplateUrl == 'feedback.html'}"><a href="#/feedback/feedback">Feedback</a></dd>
      <!-- <dd ng-class="{active: $route.current.loadedTemplateUrl == 'synthese.html'}"><a href="#/synthese/synthese">Synthese</a></dd> -->
      <?php endif; ?>
      
      <?php if(($node->nid == 8 or $node->nid == 34 or $node->nid == 93 or $node->nid == 140 or $node->nid == 172) && count(array_intersect($user->roles, $allowed)) > 0): ?> <!-- poll && screening -->
      <dd ng-class="{active: $route.current.loadedTemplateUrl == 'setting.html'}"><a href="#/instellingen">Instellingen</a></dd>
      <?php endif; ?>

    </dl>

    <div class="content"<?php print $content_attributes; ?>>

      <div ng-view>
         <!-- your processed view will show up here -->
      </div>
      
      <script type="text/ng-template" id="content.html">
            <?php if($content['field_webform_summary'] or $content['field_webform_documents']): ?>
            <div class="summary">
              <?php print render($content['field_webform_summary']); ?>
            </div>
            <?php endif; ?>

            <h2>Inhoud</h2>
            <div style="width:60%">
            <?php print render($content['field_webform_content']); ?>

            <?php if($content['field_webform_oefening']): ?>
            <h2>Oefening</h2>
            <?php print render($content['field_webform_oefening']); ?>
            <?php endif; ?>

            <?php if($content['field_webform_manual']): ?>
            <h2>Werkwijze</h2>
            <?php print render($content['field_webform_manual']); ?>
            <?php endif; ?>
            </div>
      </script>

      <script type="text/ng-template" id="exercise.html">
        <?php if($node->nid == 96 or $node->nid == 97): ?>

          <div class="exercise popup" id="exercise">
            <a href="#/inhoud" class="close" >X</a>
            <div id="print" onClick="window.print()"></div>
            <?php include 'webform_exercise_'.$node->nid.'.inc'; ?>
          </div>

        <?php elseif($node->nid == 41 or $node->nid == 173): ?>

          <div class="exercise popup" id="exercise" >
            <a href="#/inhoud" class="close" >X</a>
            <?php print render($content['webform']); ?>
          </div>

        <?php else: ?>

          <h2>Oefening</h2>
          <div id="exercise" ng-init="init(<?php echo $user->uid; ?>, <?php echo $node->nid; ?>, <?php echo $user->field_user_school['und'][0]['nid']; ?>)">
            <div ng-show="loading">Bezig met laden ...</div>
            <div ng-hide="loading">
              <?php if($content['webform']): ?>
              <?php print render($content['webform']); ?>
              <?php else: ?>
              <?php echo 'Geen digitale versie beschikbaar.'; ?>
              <?php endif; ?>
            </div>
          </div>

        <?php endif; ?>

      </script>

      <script type="text/ng-template" id="database.html">
          <h2>Database</h2>
          <div id="database">
          <?php include 'webform_database.inc'; ?>
          </div>
          
      </script>

      <script type="text/ng-template" id="method.html">

          <?php
            
            $result = db_query("select * from field_data_field_webform_methods as f_m
left join node as n on n.nid = f_m.field_webform_methods_nid
left join field_data_field_webform_gewicht as f_w on f_w.entity_id = n.nid
where f_m.entity_id = ".$node->nid."
order by f_w.field_webform_gewicht_value");

          ?>

          <h2>Alternatieve Methodieken</h2>
          <div id="methods">
          
          <?php
            if($content['field_webform_methods']): 
            foreach ($result as $record):
              $method = node_load($record->nid);
              $teaser = node_view($method, "teaser");
              echo render($teaser);
            endforeach;
            endif;
            ?>
          </div>
          
          <?php 
            if(!$content['field_webform_methods']): 
              echo 'Geen methodieken gevonden';
            endif; 
          ?>
      </script>

      <script type="text/ng-template" id="tipstricks.html">
          <h2>Tips & Tricks</h2>
          <?php if($content['field_webform_tips_tricks']): ?>
            <?php print render($content['field_webform_tips_tricks']); ?>
          <?php else: ?>
            Geen Tips & Tricks gevonden.
          <?php endif; ?>
      </script>

      <script type="text/ng-template" id="result.html">
          <h2>Resultaten</h2>
          <?php include 'webform_results.inc'; ?>
      </script>

      <script type="text/ng-template" id="setting.html">
          <h2>Instellingen</h2>
          <?php include 'webform_settings.inc'; ?>
      </script>

      <script type="text/ng-template" id="feedback.html">
          <h2>Feedback</h2>
          <?php include 'webform_comment.inc'; ?>
      </script>

      <script type="text/ng-template" id="synthese.html">
          <h2>Synthese</h2>
          <?php include 'webform_comment.inc'; ?>
      </script>

    </div>
    <?php 

    drupal_add_css(libraries_get_path('jquery-ui') . '/themes/smoothness/jquery-ui.css');
    drupal_add_js(libraries_get_path('jquery-ui') . '/ui/jquery-ui.js');
    drupal_add_js(libraries_get_path('angular') . '/angular.min.js');
    drupal_add_js(libraries_get_path('angular-route') . '/angular-route.min.js');
    drupal_add_js(libraries_get_path('angular-sanitize') . '/angular-sanitize.min.js');
    drupal_add_js(libraries_get_path('dragdrop') . '/draganddrop.js');
    //drupal_add_js(libraries_get_path('angular-ui') . '/build/angular-ui.min.js');
    drupal_add_js(libraries_get_path('angular-ui-date') . '/src/date.js');
    //drupal_add_js(libraries_get_path('angular-google-chart') . '/ng-google-chart.js');
    
    drupal_add_js(drupal_get_path('module', 'obh_exercises') . '/scripts/app.js');
    $allowed = array('begeleider', 'administrator');
    if (count(array_intersect($user->roles, $allowed)) > 0) drupal_add_js(drupal_get_path('module', 'obh_exercises') . '/scripts/route_admin.js');
    else drupal_add_js(drupal_get_path('module', 'obh_exercises') . '/scripts/route_user.js');  
    drupal_add_js(drupal_get_path('module', 'obh_exercises') . '/scripts/controllers.js');
    drupal_add_js(drupal_get_path('module', 'obh_exercises') . '/scripts/'.$node->nid.'_controllers.js');

    ?>
  <?php endif; ?>

  <?php if($view_mode == 'teaser'): ?>

    <div class="method">  
      <?php print render($content['field_webform_intro']); ?>
      <a href="<?php echo $node_url; ?>">Meer informatie</a>
    </div>
  
  <?php endif; ?>

</div>



