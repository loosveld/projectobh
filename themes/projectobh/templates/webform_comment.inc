<div ng-init="init(<?php echo $node->nid; ?>, <?php echo $user->uid; ?>)" id="comments" >
        
  <div ng-show="loading">Bezig met laden ...</div>
  <div ng-show="!comments.length && !loading" >Geen berichten gevonden</div>

  <div class="comment" ng-repeat="comment in comments | orderBy:'created':reverse">

    <h3 ng-hide="comment.editMode">{{comment.subject}}</h3>
    <input value="{{comment.subject}}" ng-show="comment.editMode" ng-model="comment.subject" type="text" size="60" maxlength="64" class="form-text">

    <div class="submitted">Ingezonden door <a href="/user/{{comment.uid}}">{{comment.name}}</a> op {{comment.created*1000 | date:'dd-MM-yy, H:mm'}}</div>
    
    <div ng-bind-html="comment.comment_body.und[0].value" ng-hide="comment.editMode"></div>
    <textarea ng-model="comment.comment_body.und[0].value" ng-show="comment.editMode" ></textarea>

    <div class="actions" ng-show="checkPermission()">
      <a ng-hide="comment.editMode" ng-click="toggleEdit()" class="button tiny radius">Bewerken</a>
      <a ng-show="comment.editMode" ng-click="commentSave()" class="button tiny radius">Bewaren</a>
      <a ng-show="comment.editMode" ng-click="toggleEdit()" class="button tiny radius">Sluiten</a>
      <a ng-click="commentDelete()" class="button tiny radius">Verwijderen</a>
    </div>
    
  </div>

  <div>
    
    <form name="formCommentNew">
      <fieldset>
      <legend>Voeg een bericht toe</legend>
      <input ng-model="commentNew.subject" type="text" name="subject" required placeholder="Onderwerp" >
      <small ng-show="formCommentNew.subject.$dirty && formCommentNew.subject.$error.required" class="error">Vul een onderwerp in</small>
      
      <textarea ng-model="commentNew.comment_body.und[0].value" name="body" required placeholder="Bericht"></textarea>
      <small ng-show="formCommentNew.body.$dirty && formCommentNew.body.$error.required" class="error">Dit veld is verplicht in te vullen</small>
      

      <a ng-click="commentAdd()" class="button tiny radius">voeg toe</a>
      </fieldset>

    </form>
  </div>
  
</div>