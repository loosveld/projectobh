
<div ng-init="init(<?php echo $user->uid; ?>, <?php echo $node->nid; ?>, <?php echo $school; ?>)" id="results" >

  <div class="filter" ng-hide="loading">
  <?php 
    $allowed = array('begeleider', 'administrator');
    if (count(array_intersect($user->roles, $allowed)) > 0):
  ?>

    <select ng-model="schoolFilter"  ng-options="school.title + ' (' + calcSubmissions(schools, school) + ')' for school in schools" ng-change="changeSchoolFilter()">
      <option value="">Alle scholen ...</option>
    </select>

  <?php
   endif;
  ?>
  
  <?php 
    $allowed = array('beheerder','begeleider', 'administrator');
    if (count(array_intersect($user->roles, $allowed)) > 0):
  ?>
    <?php if($nid == 8): $label = 'user.anonymous'; ?>
    <?php else: $label = 'user.name'; endif; ?>

    <select ng-model="userFilter" ng-options="<?php echo $label;?> + ' (' + calcSubmissions(users, user) + ')'  for user in users | filterUserBy:schoolFilter.nid" ng-change="changeUserFilter()">
      <option value="">Alle gebruikers</option>
    </select> 

    <div class="period clearfix" >
      <label>Vanaf:</label><input type="text" ng-model="dateFrom" ui-date="dateOptions" ui-date-format date-input class="dateFrom"/>
      <label>Tot:</label><input type="text" ng-model="dateTo" ui-date="dateOptions" ui-date-format date-input />
    </div>

  <?php
    endif;
  ?>
  </div>

  <!--//////////////////////-->
  <!--//// LEVEL SCHOOL ////-->
  <!--//////////////////////-->

  <div ng-show="loading">Bezig met laden ...</div>

  <div class="school" ng-hide="loading || userFilter">
    
    <?php if($node->nid == 37 or $node->nid == 58 or $node->nid == 11): ?>

      <div ng-hide="loading">Er werden geen resultaten per school gevonden voor deze oefening.</div>
    
    <?php else: ?>

      <div class="submissions" ng-hide="loading">Totaal inzendingen: {{schoolSubmissions}}</div>

      <div ng-repeat="component in data_components | orderObjectBy:'pweight' | filter:schoolFilter.title" class="clearfix result" >
          
        <?php if($node->nid == 8): //poll ?>
          {{component.pname}}
          <div ng-repeat="item in component.components | orderObjectBy:'pweight' | filter:schoolFilter.title" class="clearfix result" >
          <table cellpadding="0" cellspacing="0" style="border:0;margin-bottom:0">
          <tr>
            <td style="width:60%;padding:0;">
            {{item.pname}}
            </td>
            <td style="padding:0;padding-left:1em;">
              <div class="linebar" >
                <span ng-show="item.yes" class="green" style="float:none;width:{{(item.yes/(item.yes + item.no))*100}}%;">{{(item.yes/(item.yes + item.no))*100 | number:0}}%</span>
                <span ng-show="item.no" class="red" style="float:none;width:{{(item.no/(item.yes + item.no))*100}}%;">{{(item.no/(item.yes + item.no))*100 | number:0}}%</span>
              </div>
            </td>
          </tr>
          </table>
          </div>
        <?php endif; ?>

        <?php if($node->nid == 133  or $node->nid == 147): //werkinstrument ?>
          {{component.pname}}
          <div ng-repeat="item in component.components | orderObjectBy:'pweight' | filter:schoolFilter.title" class="clearfix result" >
            {{item.pname}}
            <table cellpadding="0" cellspacing="0" style="margin-bottom:0">
            <tr style="background:white;width:100%;">
                <td width="50%" style="text-align:right;padding:0;background-color:#ccc;"><span ng-if="item.percentage <= 0" style="background-color:white;margin-right:{{100-(item.percentage*2+100)}}%;">&nbsp;&nbsp;</span></td>
                <td width="50%" style="padding:0;background-color:#ccc;"><span ng-if="item.percentage > 0" style="background-color:#fff;margin-left:{{item.percentage*2}}%">&nbsp;&nbsp;</span></td>
            </tr>
            </table>
          </div>
        <?php endif; ?>

        <?php if($node->nid == 34 or $node->nid == 93): //screening ?> 
          {{component.pname}}
          <div class="value">
          <table cellpadding="0" cellspacing="0" style="border:0;margin-bottom:0;padding:0;">
          <tr ng-repeat="value in component.data | orderObjectBy:'cweight'" >
            <td style="width:60%;">{{value.cname}}</td>
            <td >
              <div class="linebar" >
                <span ng-show="value.level1" class="level1 reset" style="width:{{(value.level1/(value.level1 + value.level2 + value.level3 + value.level4))*100}}%;">--</span>
                <span ng-show="value.level2" class="level2 reset" style="width:{{(value.level2/(value.level1 + value.level2 + value.level3 + value.level4))*100}}%;">-</span>
                <span ng-show="value.level3" class="level3 reset" style="width:{{(value.level3/(value.level1 + value.level2 + value.level3 + value.level4))*100}}%;">+</span>
                <span ng-show="value.level4" class="level4 reset" style="width:{{(value.level4/(value.level1 + value.level2 + value.level3 + value.level4))*100}}%;">++</span>
              </div>
            </td>
          </tr>
          </table>
          </div>
        <?php endif; ?>

        <?php if($node->nid == 59 or $node->nid == 99 or $node == 117): //+/- ?>
          <table cellpadding="0" cellspacing="0" style="border:0;margin-bottom:0">
          <tr>
            <td style="width:70%;padding:0;">
              <div ng-repeat="value in component.data | orderObjectBy:'cweight'" >
                <span ng-if="value.cid == 2">
                  <h3>Acties</h3>
                  <div ng-repeat="actie in value.values | filter:schoolFilter.title" style="margin-bottom:1em;background-color:#efefef;padding:10px;" >{{actie.value}}</div>
                </span>
              </div>
            </td>
            <td style="padding:0;padding-left:1em;">
              <div ng-repeat="value in component.data | orderObjectBy:'cweight'" >
                <span ng-if="value.cid == 3">
                <h3>Waarderingen</h3>
                <div class="linebar" >
                  <span ng-show="value.level1" class="level1" style="width:{{(value.level1/(value.level1 + value.level2 + value.level3 + value.level4))*100}}%;">-- # {{value.level1}}</span>
                  <span ng-show="value.level2" class="level2" style="width:{{(value.level2/(value.level1 + value.level2 + value.level3 + value.level4))*100}}%;">- # {{value.level2}}</span>
                  <span ng-show="value.level3" class="level3" style="width:{{(value.level3/(value.level1 + value.level2 + value.level3 + value.level4))*100}}%;">+ # {{value.level3}}</span>
                  <span ng-show="value.level4" class="level4" style="width:{{(value.level4/(value.level1 + value.level2 + value.level3 + value.level4))*100}}%;">++ # {{value.level4}}</span>
                </div>
                </span>
              </div>
            </td>
          </tr>
          </table>
        <?php endif; ?>

      </div>

    <?php endif; ?>

  </div>

  <!--//////////////////////-->
  <!--// LEVEL INDIVIDUAL //-->
  <!--//////////////////////-->

  <div class="individual" ng-hide="loading || !userFilter">

    <div class="submissions" ng-hide="loading">Totaal inzendingen: {{userSubmissions}}</div>

    <div ng-repeat="submission in data_submission | orderObjectBy:'cweight' | filter:userFilter.name | dateRange: dateFrom: dateTo" class="result" ng-model="results" >
      <?php if($nid != 8): ?>
      <h3>Ingezonden door {{submission.uname}} 
      <?php endif; ?>

      op {{submission.submitted *1000 | date:'d-MM-yy, Humm'}}</h3>

        <div ng-repeat="data in submission.data | filter:userFilter.name | filter:schoolFilter.title | orderObjectBy:'submitted':'reverse'"  >

          <?php if($node->nid == 8 or $node->nid == 101): ?>
            
              <h3>{{data.pname}}</h3>
              <div class="value">
                <span ng-repeat="component in data.components | orderObjectBy:'pweight'">
                  {{component.pname}}
                  <div style="background:white;width:100%;padding:0.5em">
                    <span ng-repeat="child in component.components | orderObjectBy:'pweight'" >
                      <span ng-switch on="child.value" ng-if="child.ctype == 'select'">
                         <span ng-switch-when="yes">Ja</span>
                         <span ng-switch-when="no">Nee</span>
                         <span ng-switch-when="">Geen antwoord</span>
                         <span ng-switch-default>{{child.value}}</span>
                      </span>
                      <?php if($node->nid ==8): ?>
                      <span ng-if="child.ctype == 'textarea' && child.value">, {{child.value}}</span>
                      <?php endif; ?>
                      <?php if($node->nid ==101): ?>
                      <span ng-if="child.ctype == 'textarea' && child.value">{{child.value}}</span>
                      <?php endif; ?>
                    </span>
                  </div>
                </span>
              </div>
            
          <?php endif;?>

          <?php if($node->nid == 37): ?>
            
              <div style="padding: 10px 0px 5px 0px;font-weight: bold;">{{data.pname}}</div>
              <div class="value">
                <span ng-repeat="component in data.components | orderObjectBy:'pweight'">

                  <div>
                    --{{component.pname}}<br/>
                    <table cellpadding="0" cellspacing="0">
                    <tr ng-repeat="child in component.components | orderObjectBy:'pweight'" style="background:white;width:100%;">
                        <td style="width:20%;"><span>{{child.cname}}</span></td>
                        <td style="vertical-align:top"><span>{{child.value}}</span></td>
                    </tr>
                    </table>
                  </div>
             
                </span>
              </div>
            
          <?php endif;?>

          <?php if($node->nid == 133 or $node->nid == 147): ?>
            
              <div style="padding: 10px 0px 5px 0px;font-weight: bold;" ng-show="{{data.components}}">{{data.pname}}</div>
              <div class="value">

                <span ng-repeat="component in data.components | orderObjectBy:'pweight'">

                  <div ng-if="component.components">
                    <span ng-show="component.pname">{{component.pname}}</span><br/>
                    <table cellpadding="0" cellspacing="0">
                    <tr ng-repeat="child in component.components | orderObjectBy:'pweight'" style="background:white;width:100%;">
                        <td width="50%" style="text-align:right;padding:0;background-color:#ccc;"><span ng-if="child.value <= 0" style="background-color:white;margin-right:{{100-(child.value*2+100)}}%;">&nbsp;&nbsp;</span></td>
                        <td width="50%" style="padding:0;background-color:#ccc;"><span ng-if="child.value > 0" style="background-color:#fff;margin-left:{{child.value*2}}%">&nbsp;&nbsp;</span></td>
                    </tr>
                    </table>
                  </div>
             
                </span>

                <span ng-repeat="component in data.components | orderObjectBy:'pweight'">

                  <div ng-if="component.value"  style="margin-bottom: 1.25rem;">
                    <span ng-show="component.cname">{{component.cname}}</span><br/>
                    <div style="background:white;width:100%;padding:10px;">
                        <span>{{component.value}}</span>
                    </div>
                  </div>
             
                </span>

              </div>
            
          <?php endif;?>

          <?php if($node->nid == 58 or $node->nid == 59 or $node->nid == 99 or $node->nid == 117): ?>
            <table cellpadding="0" cellspacing="0" style="background-color:#efefef;margin:0">
              <tr ng-repeat="component in data.components | orderObjectBy:'pweight'" style="width:100%;">
                <td style="width:20%;vertical-align:top;"><span>{{component.cname}}</span></td>
                <td style="vertical-align:top"><span>{{component.value}}</span></td>
              </tr>
            </table>   
          <?php endif;?>

          <?php if($node->nid == 34): ?>
            {{data.pname}}<br/>
            <div class="value">
              <span ng-repeat="component in data.components" style="display:block">
                <table cellpadding="0" cellspacing="0" style="margin:0;border:0px; border-bottom:1px solid lightgrey" >
                <tr>
                  <td>{{component.cname}}</td>
                  <td  width="10%;" class="linebar" ng-bind-html="color(component.value)" style="padding:0px;"></td>
                </tr>
                </table>
              </span>
            </div>
          <?php endif;?>

          <?php if($node->nid == 11): ?>
            <span ng-repeat="component in data.components" style="display:block;margin-bottom: 1em;">
              {{component.cname}}
              <div class="value" style="padding: 0.5em;">
                {{component.value}}
              </div>
            </span>
          <?php endif;?>
        
        </div>

    </div>

  </div>

</div>