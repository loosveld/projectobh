<?php global $language; ?>

<div id="content">
	
	<?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>

	<?php if ($page['header']): ?>
		<?php print render($page['header']); ?>
	<?php endif; ?>
				
</div>
