<div ng-controller="DatabaseCtrl" ng-init="init(<?php echo $node->nid;?>)" id="list" >
  <?php $allowed = array('begeleider', 'administrator'); ?>
  <?php if(count(array_intersect($user->roles, $allowed)) > 0): ?>
  <div class="content-system" style="margin:0; padding:0">
    <ul class="action-links">
      <li><a href="/interactie/143/create/article">+ Artikel Toevoegen</a></li>
      <li><a ng-click="toggleTheme()">+ Thema Toevoegen</a></li>
    </ul>
  </div>
  <?php endif; ?>

  <div ng-show="showTheme" class="filter">
    <div ng-hide="showThemeMessage"><input type="text" ng-model="newTheme" /><input type="button" ng-click="save()" value="Thema Toevoegen" /></div>
    <div ng-show="showThemeMessage" style="color:#23b352;">Het thema {{newTheme}} werd toegevoegd</div>
  </div>
    
  <div class="filter clearfix">

    <select ng-model="theme" ng-hide="loading" ng-options="theme.name for theme in themes" class="theme" >
      <option value="">Kies een thema ...</option>
    </select>

    <!-- <div ng-hide="loading" style="float:right;margin-right:10px;"><label style="float:left">Zoeken ... </label><input ng-model="search" type="text" /></div> -->
    
    <div ng-show="loading">Bezig met laden ...</div>

    <div ng-hide="loading" style="line-height:35px;
margin-top: 0px;float:left;">Resultaten: <b>{{(list|filter:theme.title|filter:search|filter:type).length}}</b></div>

  </div> 

  <table class="views-table" ng-hide="loading">
    <thead>
      <tr>
        <th>Artikel</th>
        <th></th>
        <th>Thema</th>
        <?php if(count(array_intersect($user->roles, $allowed)) > 0): ?><th>Actie</th><?php endif; ?>
      </tr>
    </thead>
    <tbody>
      <tr id="node-{{item.nid}}" ng-repeat="item in list | filter:theme.name | filter:search | filter:type" ng-hide="loading">
     
        <td valign="top" class="views-field" width="50%">{{item.title}}</td>
        <td valign="top" width="25%"><a href="{{item.url}}" ng-show="item.url" target="_blank">Link</a> &nbsp;<a href="{{item.document}}" ng-show="item.document" target="_blank">Document</a></td>
        <td valign="top" width="25%">{{item.theme.title}}</td>
        <?php if(count(array_intersect($user->roles, $allowed)) > 0): ?><td align="center"><a href="#">X</a></td><?php endif; ?>

      </tr>
    </tbody>
  </table>


  
</div>