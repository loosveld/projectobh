<div id="webform-client-form-96" ng-init="init()">
  <div class="part one" >
    <h1>Bouwsteen 1</h1><h2>Partnerschap</h2>
    <div class="triangle"><a ng-click="section(1)">Driehoeksverhouding</a></div>
  </div>
  <div class="part two">
    <h1>Bouwsteen 2</h1><h2>De Drie Pijlers</h2>
    <div class="square" >Eerste pijler<br/><b><a ng-click="section(2)">Doel: maximale ontwikkelingskansen</a></b></div>
    <div class="square" >Tweede pijler<br/><b>Acties voor een krachtige leeromgeving:</b><br/>
      <a ng-click="section(10)">INSTEEK 1</a> – Contact: klassiek - divers<br/><a ng-click="section(11)">INSTEEK 2</a> – Onrechtstreeks - rechtstreeks<br/><a ng-click="section(12)">INSTEEK 3</a> – Formeel - informeel<br/><a ng-click="section(13)">INSTEEK 4</a> – Doelgroepouders - álle ouders<br/><a ng-click="section(14)">INSTEEK 5</a> – Probleemgerichte insteek - positieve insteek<br/><a ng-click="section(15)">INSTEEK 6</a> – Adhoc - geïntegreerd in beleid
    </div>
    <div class="square" >
      Derde pijler<br/>
      <b><a ng-click="section(4)">Percepties en verwachtingen</a></b><ul><li>over eigen willen en kunnen</li><li>over andermans willen en kunnen</li></ul>
      <b><a ng-click="section(16)">Engagementen</a></b><ul><li>in de thuiscontext</li><li>op school</li></ul>
    </div>
  </div>
  <div class="part three">
    <h1>Bouwsteen 3</h1><h2>Noodzakelijke voorwaarden</h2>
    <div class="square" ><a ng-click="section(5)">In de spiegel kijken</a><br/>Eigen referentiekader in vraag stellen</div>
    <div class="square" ><a ng-click="section(6)">Ken uw ouders (en hun kinderen)!</a><br/>Breed verkennen</div>
    <div class="square" ><a ng-click="section(17)">Diversiteit als meerwaarde</a></div>
    <div class="square" ><a ng-click="section(18)">Draagvlak</a></div>
    <div class="square" ><a ng-click="section(7)">Basishouding</a></div>
  </div>
<!--   <div class="part four" ng-click="section(8)">
    <h1>Bouwstenen 4 - <b>ZES TOETSSTENEN</b></h1>
  </div> -->
</div>

<div class="info">

  <div class="content global" ng-show="is(0)">
    <h1><?php echo $content['webform']['#node']->webform['components'][1]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][1]['value']; ?>
  </div>
  <div class="content one" ng-show="is(1)">
    <h1><?php echo $content['webform']['#node']->webform['components'][2]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][2]['value']; ?>
  </div>
  <div class="content one" ng-show="is(2)">
    <h1><?php echo $content['webform']['#node']->webform['components'][3]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][3]['value']; ?>
  </div>
  <div class="content one" ng-show="is(3)">
    <h1><?php echo $content['webform']['#node']->webform['components'][4]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][4]['value']; ?>
  </div>
  <div class="content one" ng-show="is(10)">
    <h1><?php echo $content['webform']['#node']->webform['components'][10]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][10]['value']; ?>
  </div>
  <div class="content one" ng-show="is(11)">
    <h1><?php echo $content['webform']['#node']->webform['components'][11]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][11]['value']; ?>
  </div>
  <div class="content one" ng-show="is(12)">
    <h1><?php echo $content['webform']['#node']->webform['components'][12]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][12]['value']; ?>
  </div>
  <div class="content one" ng-show="is(13)">
    <h1><?php echo $content['webform']['#node']->webform['components'][13]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][13]['value']; ?>
  </div>
  <div class="content one" ng-show="is(14)">
    <h1><?php echo $content['webform']['#node']->webform['components'][14]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][14]['value']; ?>
  </div>
  <div class="content one" ng-show="is(15)">
    <h1><?php echo $content['webform']['#node']->webform['components'][15]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][15]['value']; ?>
  </div>
  <div class="content one" ng-show="is(4)">
    <h1><?php echo $content['webform']['#node']->webform['components'][5]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][5]['value']; ?>
  </div>
  <div class="content one" ng-show="is(16)">
    <h1><?php echo $content['webform']['#node']->webform['components'][16]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][16]['value']; ?>
  </div>
  <div class="content one" ng-show="is(5)">
    <h1><?php echo $content['webform']['#node']->webform['components'][6]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][6]['value']; ?>
  </div>
  <div class="content one" ng-show="is(6)">
    <h1><?php echo $content['webform']['#node']->webform['components'][7]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][7]['value']; ?>
  </div>
  <div class="content one" ng-show="is(7)">
    <h1><?php echo $content['webform']['#node']->webform['components'][8]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][8]['value']; ?>
  </div>
  <div class="content one" ng-show="is(8)">
    <h1><?php echo $content['webform']['#node']->webform['components'][9]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][9]['value']; ?>
  </div>

  <div class="content one" ng-show="is(18)">
    <h1><?php echo $content['webform']['#node']->webform['components'][17]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][17]['value']; ?>
  </div>

  <div class="content one" ng-show="is(17)">
    <h1><?php echo $content['webform']['#node']->webform['components'][18]['name']; ?></h1>
    <?php echo $content['webform']['#node']->webform['components'][18]['value']; ?>
  </div>

</div>