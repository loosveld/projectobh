<div ng-init="init(0, <?php echo $node->nid; ?>)" id="components" >
  
  <div ng-show="loader1">Bezig met laden ...</div>

  <select ng-model="schoolFilter" ng-hide="loader1" ng-options="school.title for school in schools" >
    <option value="">Kies een school ...</option>
  </select>
    
  <table cellpadding="0" cellspacing="0" >
    <tr ng-repeat="component in clist | filter: filterSchool">
      <td>
        <span ng-if="component.pid == 0"><h3>{{component.name}}</h3></span>
        <span ng-if="component.pid != 0">{{component.name}}</span>
      </td>
      <td>
          <input type="checkbox" ng-model="component.visible" ng-click="saveComponentSetting($event)">
      </td>
    </tr>
  </table>

</div>