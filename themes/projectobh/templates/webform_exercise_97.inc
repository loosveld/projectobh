<div id="webform-client-form-97" >
 
  <div class="part one" ui-on-Drop="onDrop($event, $data, [1,2])">
    <h1>Bouwsteen 1</h1><h2>Partnerschap</h2>
    <div class="triangle">Driehoeksverhouding</div>
  </div>
  <div class="part two" >
    <h1>Bouwsteen 2</h1><h2>De Drie Pijlers</h2>
    <div class="square" ui-on-Drop="onDrop($event, $data, [3,4])">Eerste pijler<br/><b>Doel: maximale ontwikkelingskansen</b></div>
    <div class="square" >Tweede pijler<br/><b>Acties voor een krachtige leeromgeving:</b><br/>
      <p ui-on-Drop="onDrop($event, $data, [5])" style="background-color:#eee;padding:10px">INSTEEK 1 – Contact: klassiek - divers</p>
      <p ui-on-Drop="onDrop($event, $data, [6])" style="background-color:#eee;padding:10px">INSTEEK 2 – Onrechtstreeks - rechtstreeks<br/></p>
      <p ui-on-Drop="onDrop($event, $data, [7])" style="background-color:#eee;padding:10px">INSTEEK 3 – Formeel - informeel<br/></p>
      <p ui-on-Drop="onDrop($event, $data, [8])" style="background-color:#eee;padding:10px">INSTEEK 4 – Doelgroepouders - álle ouders<br/></p>
      <p ui-on-Drop="onDrop($event, $data, [9])" style="background-color:#eee;padding:10px">INSTEEK 5 – probleemgerichte insteek - positieve insteek<br/></p>
      <p ui-on-Drop="onDrop($event, $data, [10])" style="background-color:#eee;padding:10px">INSTEEK 6 – Adhoc - geïntegreerd in beleid</p>
    </div>
  </div>
  <div class="part three">
    <h1>Bouwsteen 3</h1><h2>Noodzakelijke voorwaarden</h2>
    <div class="square" >
      Derde pijler<br/>
      <p ui-on-Drop="onDrop($event, $data, [11])" style="background-color:#eee;padding:10px;margin:0"><b>Percepties en verwachtingen</b><ul><li>over eigen willen en kunnen</li><li>over andermans willen en kunnen</li></ul></p>
      <p ui-on-Drop="onDrop($event, $data, [12])" style="background-color:#eee;padding:10px;margin:0"><b>Engagementen</b><ul><li>in de thuiscontext</li><li>op school</li></ul></p>
    </div>
  </div>

</div>

<div class="info">
  <br/><br/><br/>
  <div class="content global">
    <p>Selecteer een citaat en sleep het naar het juiste onderdeel.</p>
    <a ng-repeat="quote in quotes track by $index" ng-click="toggle($index)" ui-draggable="true" drag="quote.id" on-drop-success="dropSuccessHandler($event,$index,quotes)" class="rounded" ng-class="{selected:quote.visible}">
        Citaat {{$index+1}}
    </a>
    <br style="clear:both" /><br style="clear:both" />
    <ul>
      <li class="{{quote.quote}} rounded" ui-draggable="true" drag="quote.id" 
        on-drop-success="dropSuccessHandler($event,$index,quotes)"
        ng-repeat="quote in quotes track by $index" ng-show="quote.visible">
        <h2 style="margin-top:0">Citaat:</h2>
        {{quote.quote}}
      </li>
    </ul>

  </div>
  <br/>
  <div class="answer color_{{answer}} rounded" ng-show="showAnswer"><br/><div ng-hide="answer">Uw keuze is foutief!</div><div ng-show="answer">Uw keuze is juist!</div><br/></div>
  
</div>
