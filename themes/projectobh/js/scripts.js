(function ($) {

	Drupal.behaviors.NAME = {
	attach: function(context, settings) {
      	//BACKGROUND IMAGE STRETCH
      	$.backstretch("/sites/projectobh.dev/themes/projectobh/images/background_image_1.jpg");
      			
      	//HOME BOX CLICK FUNCTIONALITY
      	$('.front .node-module, #sidebar .module_title').on('click',function(){
      	if($(this).hasClass('node-promoted') || ($(this).hasClass('node') && $(this).hasClass('block'))) window.location.href = $(this).data('url');
            });

      	//PRINT FUNCTIONALITY
            $("#print").on("click", function(){
            	window.print();
            });

            //EXERCISE OVERLAY
            $("#overlay, .popup").on('click', function(){
                  console.log('click');
            	$('.overlay').hide();
                  $('.popup').hide();
            });

            //SCREENING LEERKRACHT EN SCHOOL/DOEL EN PROBLEEMBOOL UITSPLITSING 
            url = window.location.href;
            var urlAux = url.split('/');

            $(".menu-block-7 .menu, .menu-block-14 .menu").prepend('<li id="screening" style="cursor:pointer"><a class="parent">Screening</a></li>');
            $(".menu-block-8 .menu").prepend('<li id="tree" style="cursor:pointer"><a class="parent">Problemen- en doelenboom</a></li>');

            var li = $(".menu-block-7 .menu-mlid-529 a, .menu-block-14 .menu-mlid-786 a").addClass('sub').clone();
            $("#screening").append(li.hide());
            $(".menu-block-7 .menu-mlid-529, .menu-block-14 .menu-mlid-786").hide();

            var li = $(".menu-block-7 .menu-mlid-741 a, .menu-block-14 .menu-mlid-809 a").addClass('sub').clone();
            $("#screening").append(li.hide());
            $(".menu-block-7 .menu-mlid-741, .menu-block-14 .menu-mlid-809").hide();

            var li = $(".menu-block-8 .menu-mlid-536 a").addClass('sub').clone();
            $("#tree").append(li.hide());
            $(".menu-block-8 .menu-mlid-536").hide();

            var li = $(".menu-block-8 .menu-mlid-810 a").addClass('sub').clone();
            $("#tree").append(li.hide());
            $(".menu-block-8 .menu-mlid-810").hide();

            $("#screening a.parent, #tree a.parent").on('click', function(){
                  $("#screening a.sub, #tree a.sub").toggle();
            });
       
            if(urlAux[4] == '34' || urlAux[4] == '93' || urlAux[4] == '140' || urlAux[4] == '172' || urlAux[4] == '41' || urlAux[4] == '173'){
               $("#screening, #tree").find('a.sub').show();   
            } 
            else{
               $("#screening, #tree").find('a.sub').hide();   
            } 

            //FANCYBOX
            $(".fancybox").fancybox().trigger('click');

            //INLINE Labels
            $("input[type=text]").cc_inlineFields();
            $("input[type=password]").cc_inlineFields();
            
            //AUTO REPLACE MAIL/USERNAME INPUT VALUE
            $('#edit-account #edit-mail').on('keyup keypress blur change click', function(){
            	$('#edit-account #edit-name').val($(this).val());
            });

	}
  };
	
})(jQuery);