<?php			
	if(drupal_is_front_page()) {
		unset($page['content']['system_main']['default_message']);
	}
?>

<nav id="header">
	<?php print render($page['header']); ?>		
</nav>

<div id="container">
		
	<section id="main" class="clearfix" >
		
		<div id="content">

			<div class="content-system">

				<?php if ($messages): ?>
					<div id="messages">
						<?php print $messages; ?>
					</div>
				<?php endif; ?>
														
				<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

			</div>
	
			<?php if ($page['content']): ?>
				<?php print render($page['content']); ?>
			<?php endif; ?>
			
		</div>
				  
	</section>
	<div class="push"></div>
	
</div>

<div id="footer">
	<?php print render($page['footer']); ?>
</div>
