<?php

function projectobh_preprocess_html(&$vars) {
  unset($vars['classes_array'][3]);

	global $user;
  $roles = array('begeleider', 'administrator');
  if(count(array_intersect($user->roles, $roles)) > 0):
    $vars['classes_array'][] = 'begeleider';
  else:
    $vars['classes_array'][] = 'beheerder';
  endif;

	if (!empty($vars['page']['sidebar'])) {
    $vars['classes_array'][] = 'sidebar';
  }else{
  	$vars['classes_array'][] = 'no-sidebar';
  }

	$nid = _get_module_id(1);
	$node = node_load($nid);

	if($node->type == 'module'):
		$vars['classes_array'][] = "module_".$node->nid;
	endif;
}

function projectobh_breadcrumb($vars) {  
  $output = '';
  global $node;

  $node = node_load(arg(1));

  $active_title = menu_get_active_title();

  $breadcrumb =  $vars['breadcrumb'];

  if($node->field_webform_type['und'][0]['value'] == 'method'){
    $str = $breadcrumb[3];
    if (preg_match('/"([^"]+)"/', $str, $m)) {
      $url = $m[1];
      array_push($breadcrumb, '<a href="'.$url.'#/methodieken">Alternatieve Methodieken</a>');
    }
  }

  array_shift($breadcrumb);
  
  if (!empty($breadcrumb) ) {
    $output .= implode('&nbsp;»&nbsp;', $breadcrumb) . '&nbsp;&nbsp;';//»&nbsp;<span class="active">'.$active_title.'</Span>';
    return $output;
  }else{ 
    return '<div class="active">'.$active_title.'</div>';
  
  } 

}

function projectobh_preprocess_page(&$vars) {
	$nid = _get_module_id(1);
	$node = node_load($nid);
  $alias = explode('/', request_path());
  global $user;

	if($node):
		$vars['module_title'] = $node->title;
		$vars['module_nid'] = $node->nid;
    $vars['module_type'] = $node->type;
	else:
    if(arg(0) == 'settings' or $alias[0] == 'settings' or $alias[0] == 'schools') {
      $vars['module_nid'] = 0;
      $vars['module_title'] = 'INSTELLINGEN';
      if($alias[1] == 'user') $vars['module_title'] = 'MIJN ACCOUNT';
    }
    if(arg(0) == 'user'){
      $vars['module_nid'] = 1;
      $vars['module_title'] = 'MIJN ACCOUNT';
      if((arg(2) == 'edit' or arg(2) == 'cancel' or arg(0) == 'user') && arg(1) != $user->uid){
        $vars['module_title'] = 'INSTELLINGEN';
      }
    }
	endif;

}

function projectobh_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] .= '<ul class="custom_tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

function projectobh_form_alter(&$form, &$form_state, $form_id) {

}