app.controller('ExerciseCtrl',['$scope','$routeParams', function($scope, $routeParams) {
  var section = 0;

  $scope.init = function () {
  	if($routeParams.section == null) section = 0;
  	else section = $routeParams.section;
  }

  $scope.section = function (id) {
     section = id;   
  };

  $scope.is = function (id) {
    return section == id;
  };
}]);

