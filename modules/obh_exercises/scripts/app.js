// 'use strict';



var app = angular.module('obh_exercise', ['ngRoute', 'ngSanitize', 'ui.date', 'ngDragDrop']);

app.filter('orderObjectBy', function() {
    return function(input, attribute, direction) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for (var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function(a, b) {
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            if(!direction) return a - b;
            else return b - a;
        });

        return array;
    }
});

app.filter('filterUserBy', function() {
    return function(input, attribute) {
        var array = [];

        angular.forEach(input, function(item, index){
            if(attribute){
                if(item.school == attribute){
                    array.push(item);
                }
            }else{
                array.push(item);
            }
        });

        return array;
    }
});

app.filter('dateRange', function(parse) {
    return function(input, from, to) {
        var array = [];
        angular.forEach(input, function(item, index){
            var _from = parse.date(from); 
            var _to = parse.date(to); 
            if(item.submitted){
                if(item.submitted > _from && item.submitted <= _to){
                    array.push(item);
                }
            }
        });
        return array;
    }
});

app.directive('dateInput', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.ngModel, function (v) {
                var date = new Date(v);
                element.val(date.getDate(-1)+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
            });
        }
    };
});

app.directive('dragdealer', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            id = element.parent().parent().parent().attr('id');
            var textfield = element.parent().parent().parent().find('.webform-component-number input');
            element.html('<div id="slider-'+id+'" class="dragdealer"><div class="handle red-bar"></div></div>');
            new Dragdealer('slider-'+id,{
                x: 0.5,
                animationCallback: function(x, y) {
                //$('#just-a-slider .value').text(Math.round(x * 100));
                    textfield.val(Math.round(x * 100)-50);
                }
            });
        }
    };
}); 


app.factory('parse', function(){
    return{
        date: function(date){
            if(isNaN(date)) var parsed = Date.parse(date)/1000;
            else var parsed = date/1000; 

            return parsed;
        }
    }
});

app.factory('_api', function($http) {
    return {
        get: function(type) {
            var promise = $http.get('/api/'+type).then(function (response) {
                return response.data;
            });
            return promise;
        }
    };
});



// app.factory('chart', function() {

//     var chart = {};
//     chart.type = "PieChart";
//     chart.displayed = false;
//     chart.cssStyle = "height:300px; width:300px;";

//     chart.data = {
//         "cols": [{
//             id: "month",
//             label: "Month",
//             type: "string"
//         }, {
//             id: "laptop-id",
//             label: "Laptop",
//             type: "number"
//         }, ],
//         "rows": [{
//             c: [{
//                 v: "label1"
//             }, {
//                 v: 1
//             }]
//         }, {
//             c: [{
//                 v: "label2"
//             }, {
//                 v: 2
//             }]
//         }]
//     };

//     chart.options = {
//         "title": 'ddddd',
//         "isStacked": "true",
//         "fill": 20,
//         "displayExactValues": true,
//         "vAxis": {
//             "title": "Sales unit",
//             "gridlines": {
//                 "count": 10
//             }
//         },
//         "hAxis": {
//             "title": "Date"
//         }
//     };

//     var formatCollection = [{
//         name: "color",
//         format: [{
//             columnNum: 4,
//             formats: [{
//                 from: 0,
//                 to: 3,
//                 color: "white",
//                 bgcolor: "red"
//             }, {
//                 from: 3,
//                 to: 5,
//                 color: "white",
//                 fromBgColor: "red",
//                 toBgColor: "blue"
//             }, {
//                 from: 6,
//                 to: null,
//                 color: "black",
//                 bgcolor: "#33ff33"
//             }]
//         }]
//     }, {
//         name: "arrow",
//         checked: false,
//         format: [{
//             columnNum: 1,
//             base: 19
//         }]
//     }, {
//         name: "date",
//         format: [{
//             columnNum: 5,
//             formatType: 'long'
//         }]
//     }, {
//         name: "number",
//         format: [{
//             columnNum: 4,
//             prefix: '$'
//         }]
//     }, {
//         name: "bar",
//         format: [{
//             columnNum: 1,
//             width: 100
//         }]
//     }]

//     chart.formatters = {};

//     return chart;
// });
