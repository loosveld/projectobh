app.controller('DatabaseCtrl',function($scope, $http, $routeParams, _api) {
    
    $scope.list = [];
    $scope.themes = [];
    $scope.loading = true;
    $scope.newTheme = '';
    $scope.showTheme = false;
    $scope.showArticle = false;
    $scope.showThemeMessage = false;
    
    $scope.init = function(defaultTheme){
      _api.get('article').then(function(data) {
          
          $scope.list = data;
          $scope.loading = false;
          
          _api.get('theme').then(function(data) {
            $scope.themes = data;
            
            angular.forEach($scope.list, function(item, index){
              var href = "";
              if(item.url != null) href = item.url;
              if(item.document != null) href = item.document;
              $scope.list[index].href = href; 
            });

          });
      });
    };

    $scope.save = function (){
      item = {};
      item.name = $scope.newTheme;
      $scope.post(item);
    }

    $scope.post = function (item) {
      $http.post('/api/theme', item).
        success(function(data) {
          term = {};
          term.name = $scope.newTheme;
          $scope.themes.push(term);
          $scope.showThemeMessage = true;
      });
    }

    $scope.toggleTheme = function() {
      $scope.showTheme = $scope.showTheme === false ? true: false;
    };

    $scope.toggleArticle = function() {
      $scope.showArticle = $scope.showArticle === false ? true: false;
    };

});



  // .filter('orderObjectBy', function(){
  //   return function(input, attribute) {
  //     if (!angular.isObject(input)) return input;

  //     var array = [];
  //     for(var objectKey in input) {
  //         array.push(input[objectKey]);
  //     }

  //     array.sort(function(a, b){
  //         a = parseInt(a[attribute]);
  //         b = parseInt(b[attribute]);
  //         return a - b;
  //     });
  //     return array;
  //   }
  // });

