app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/inhoud', {
            controller: 'DefaultCtrl',
            templateUrl: 'content.html',
        })
        .when('/oefening', {
            controller: 'ExerciseCtrl',
            templateUrl: 'exercise.html'
        })
        .when('/oefening/:section', {
            controller: 'ExerciseCtrl',
            templateUrl: 'exercise.html'
        })
        .when('/databank', {
            controller: 'DatabaseCtrl',
            templateUrl: 'database.html'
        })
        .when('/methodieken', {
            controller: 'DefaultCtrl',
            templateUrl: 'method.html'
        })
        .when('/tipstricks', {
            controller: 'DefaultCtrl',
            templateUrl: 'tipstricks.html'
        })
        .when('/resultaten', {
            controller: 'ResultCtrl',
            templateUrl: 'result.html'
        })
        .when('/feedback/:type', {
            controller: 'CommentCtrl',
            templateUrl: 'feedback.html'
        })
        .when('/synthese/:type', {
            controller: 'CommentCtrl',
            templateUrl: 'synthese.html'
        })
        .when('/instellingen', {
            controller: 'SettingCtrl',
            templateUrl: 'setting.html'
        })
        .otherwise({
            controller: 'DefaultCtrl',
            templateUrl: 'content.html'
        });
});

