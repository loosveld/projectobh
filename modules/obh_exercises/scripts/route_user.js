app.config(function($routeProvider, $routeParams, $locationProvider) {
    $routeProvider
        .when('/oefening', {
            controller: 'ExerciseCtrl',
            templateUrl: 'exercise.html'
        })
        .when('/oefening/:section', {
            controller: 'ExerciseCtrl',
            templateUrl: 'exercise.html'
        })
        .when('/databank/:section', {
            controller: 'DatabaseCtrl',
            templateUrl: 'database.html'
        })
        .when('/resultaten', {
            controller: 'ResultCtrl',
            templateUrl: 'result.html'
        })
        .otherwise({
            controller: 'ExerciseCtrl',
            templateUrl: 'exercise.html'
        });
});

