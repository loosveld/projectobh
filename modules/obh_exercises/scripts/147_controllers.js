app.controller('ExerciseCtrl', function($scope, $http){
  $scope.loading = true;
  $scope.nid = null;
  $scope.uid = null;
  $scope.schoolid = null;
  $scope.settings = [];
  $scope.components = [];

  $scope.init = function(uid, nid, schoolid){
    $scope.nid = nid;
    $scope.uid = uid;
    $scope.schoolid = schoolid;
    $scope.componentIndex();
  }

  $scope.componentIndex = function(){
    $http.get('/api/webform/component/147').
      success(function(data, status, headers, config) {
        angular.forEach(data, function(component, index){
          $scope.components.push(component);
          //console.log(component);

        });
      angular.forEach($scope.components, function(component, index){ 
        
          //angular.element('.cid_'+component.cid).append('<div dragdealer>dragdealer</div>');
      });
      $scope.loading = false;
    });
  }
});

// app.controller('DatabaseCtrl',['$scope','$routeParams', function($scope, $routeParams) {
    
//     $scope.list = [];
//     $scope.themes = [];
//     $scope.loading = true;
//     $scope.disableThemeFilter = false;
//     $scope.disableTypeFilter = true;
    
//     $scope.init = function(type, defaultTheme){
//       if(type == 'bron' || type == 'news' || type == 'kennisfiche' || type == 'faq') $scope.disableThemeFilter = true;
//       if(type == 'bron') $scope.disableTypeFilter = false;
//       var resource = $resource('/api/'+type);
//       var themes = $resource('/api/thema');
//       $scope.list = resource.query(function() { 
//           $scope.themes = themes.query(function() { 
//           if(defaultTheme){
//             angular.forEach($scope.themes, function(theme, index){
//               if(theme.nid == defaultTheme) $scope.theme = $scope.themes[index];
//             })
//           };
//           $scope.loading = false;
//         });

//         angular.forEach($scope.list, function(item, index){
//           $scope.list[index].safeBody = $sce.trustAsHtml(item.body); 
//         });
//         console.log($scope.list); 

//       });


//     };

//     $scope.defaultTheme = function(nid){
//       $scope.theme = $scope.themes[1];
//     };

//   })

//   .filter('orderObjectBy', function(){
//     return function(input, attribute) {
//       if (!angular.isObject(input)) return input;

//       var array = [];
//       for(var objectKey in input) {
//           array.push(input[objectKey]);
//       }

//       array.sort(function(a, b){
//           a = parseInt(a[attribute]);
//           b = parseInt(b[attribute]);
//           return a - b;
//       });
//       return array;
//     }
//   });