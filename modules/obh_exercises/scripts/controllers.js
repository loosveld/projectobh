'use strict';

app.controller('NavCtrl', function($scope, $route) {
  $scope.$route = $route;
})

app.controller('ExerciseCtrl', function($scope, $http){
  $scope.loading = true;
  $scope.nid = null;
  $scope.uid = null;

  $scope.init = function(uid, nid, schoolid){
    $scope.loading = false;
  }
});

app.controller('DefaultCtrl', function($scope){});

app.controller('ResultCtrl', function ($scope, $http, _api, parse) {
  $scope.form = {};
  $scope.loading = true;
  $scope.data_submission = [];
  $scope.data_components = [];
  $scope.userSubmissions = 0;
  $scope.schoolSubmissions = 0;
  $scope.users = [];
  $scope.schools = [];
  $scope.schoolid = null;
  $scope.uid = null;
  $scope.dateOptions = {
     dateFormat: 'dd-mm-yy'
  };
  $scope.dateCurrent = new Date();
  $scope.dateFrom = ($scope.dateCurrent.setMonth($scope.dateCurrent.getMonth()-12));
  $scope.dateTo = $scope.dateCurrent.setMonth($scope.dateCurrent.getMonth()+12);

  $scope.init = function(uid, nid, schoolid){
    $scope.uid = uid;
    $scope.nid = nid;
    $scope.schoolid = schoolid;
    $scope.resultIndex();
  }

  $scope.resultIndex = function(){
    $http.get('/api/webform/submission/'+$scope.nid).
      success(function(data, status, headers, config) {
        
        $scope.data_submission = data.submissions;
        $scope.data_components = data.components;

        //GET FILTERS READY
        _api.get('school').then(function(data) {
          $scope.schools = data;
          angular.forEach($scope.schools, function(school, index){
            if(school.nid == $scope.schoolid) $scope.schoolFilter = $scope.schools[index];
          });

          _api.get('user').then(function(data) {
            $scope.users = data;

            angular.forEach($scope.users, function(user, index){
              if(user.uid == $scope.uid) $scope.userFilter = $scope.users[index];
            });

            $scope.userSubmissions = $scope.calcSubmissions($scope.users, $scope.userFilter);
            $scope.schoolSubmissions = $scope.calcSubmissions($scope.schools, $scope.schoolFilter);
            $scope.calcPercentage();
            $scope.loading = false;

            console.log($scope.data_components);
            
          });
        });

        $scope.$watchCollection('[dateFrom, dateTo]', function() {
          $scope.calcPercentage();
          $scope.userSubmissions = $scope.calcSubmissions($scope.users, $scope.userFilter);
          $scope.schoolSubmissions = $scope.calcSubmissions($scope.schools, $scope.schoolFilter);
        });

      });   
  }

  $scope.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) size++;
    }
    return size;
  };

  $scope.changeSchoolFilter = function(){
    $scope.calcPercentage();
    $scope.userFilter = null;
    $scope.schoolSubmissions = $scope.calcSubmissions($scope.schools, $scope.schoolFilter);
  };

  $scope.changeUserFilter = function(){
    //console.log($scope.userFilter);
    $scope.userSubmissions = $scope.calcSubmissions($scope.users, $scope.userFilter);
  };

  $scope.calcSubmissions = function(array, filter){
    var _from = parse.date($scope.dateFrom); 
    var _to = parse.date($scope.dateTo); 
    var submissions = 0;
    angular.forEach(array, function(item, index){
      angular.forEach($scope.data_submission, function(submission, index){
        if(submission.submitted > _from && submission.submitted <= _to){
          if(!item.type && item.type != 'school'){
            if(filter){
              if(item.uid == submission.uid && filter.uid == submission.uid) submissions++;
            }else{
              if(item.uid == submission.uid) submissions++;
            }
          }else{
            if(filter){
              if(item.nid == submission.schoolid && filter.nid == submission.schoolid) submissions++;
            } 
          }
        }
      });
    });
    return submissions;
  };

  $scope.calcPercentage = function(){
    var _from = parse.date($scope.dateFrom); 
    var _to = parse.date($scope.dateTo); 

    angular.forEach($scope.data_components, function(item, i){

      //1level categories
      angular.forEach(item.data, function(components, j){
        components.level1 = 0;
        components.level2 = 0;
        components.level3 = 0;
        components.level4 = 0;

        angular.forEach(components.values, function(value, k){
          if(value.submitted > _from && value.submitted <= _to){
            if($scope.schoolFilter){
              if(($scope.nid == 34 || $scope.nid == 59) && value.schoolid == $scope.schoolFilter.nid){
                if(value.value == '--') components.level1++;
                if(value.value == '-') components.level2++;
                if(value.value == '+') components.level3++;
                if(value.value == '++') components.level4++;
              }
            }else{
              if(($scope.nid == 34 || $scope.nid == 59) && value.schoolid){
                if(value.value == '--') components.level1++;
                if(value.value == '-') components.level2++;
                if(value.value == '+') components.level3++;
                if(value.value == '++') components.level4++;
              }
            }
          }
        });
        
        item.data[components.cid] = components;
      });
      
      //2level categories
      angular.forEach(item.components, function(component1, k){

          component1.yes = 0;
          component1.no = 0;
          component1.percentage = 0;
          angular.forEach(component1.data, function(component2, j){
            angular.forEach(component2.values, function(value, s){
              if(value.submitted > _from && value.submitted <= _to){
                if($scope.schoolFilter){
                  if($scope.nid == 8 && value.schoolid == $scope.schoolFilter.nid){
                      if(value.value == 'yes') component1.yes++;
                      if(value.value == 'no') component1.no++;
                  }
                  if(($scope.nid == 133 || $scope.nid == 147) && value.schoolid){
                    component1.percentage += parseInt(value.value);    
                  }
                }else{
                  if($scope.nid == 8 && value.schoolid){
                    if(value.value == 'yes') component1.yes++;
                    if(value.value == 'no') component1.no++;
                  }
                  if(($scope.nid == 133 || $scope.nid == 147) && value.schoolid){
                    component1.percentage += parseInt(value.value);    
                  }
                }
              }
            });
            component1.percentage = component1.percentage/component2.values.length;
          });

      });
    });

  }

  $scope.sumValues = function(){
    return 'test';
  }

  $scope.color = function(item){
    if(item == '--') return '<span class="level1 reset">'+item+'</span>';
    if(item == '-') return '<span class="level2 reset">'+item+'</span>';
    if(item == '+') return '<span class="level3 reset">'+item+'</span>';
    if(item == '++') return '<span class="level4 reset">'+item+'</span>';
  }

});

app.controller('CommentCtrl', function ($scope, $http, $routeParams) {
  $scope.form = {};
  $scope.loading = true;
  $scope.editMode = false;
  $scope.comments = [];
  $scope.uid = 0;
  $scope.nid = 0;

  $scope.commentNew = {
    nid: 0,
    cid: 0,
    subject: '',
    is_new: 1,
    pid: 0,
    uid: 0,
    mail: '',
    is_anonymous: 0,
    homepage: '',
    status: 1,
    language: 'und',
    comment_body: {
      und: [
        {value: '', format: 'filtered_html'}
      ]
    },
    field_comment_type:{
      und: [
        {value: ''}
      ]
    }
  };

  $scope.init = function(nid, uid){
    $scope.commentNew.nid = nid;
    $scope.commentNew.uid = uid;
    $scope.commentNew.field_comment_type.und[0].value = $routeParams.type;
    $scope.commentIndex($routeParams.type, nid);
  }

  $scope.commentIndex = function(type, nid){
    $http.get('/api/comment/'+type+'/'+nid).
      success(function(data, status, headers, config) {
        $scope.comments = data;
        $scope.loading = false;
      });  
  }

  $scope.toggleEdit = function(){
    this.comment.editMode = ! this.comment.editMode;
  }
  
  $scope.commentDelete = function(){
    $scope.delete(this.comment);
  }

  $scope.commentAdd = function(){
    if($scope.formCommentNew.$valid){
      $scope.post($scope.commentNew);
    }
  }
    
  $scope.commentSave = function(){
    var comment = this.this.comment;
    $scope.post(comment);
    this.comment.editMode = ! this.comment.editMode;
  }

  $scope.post = function (comment) {
    $http.post('/api/comment', comment).
      success(function(data) {
        if(data.is_new){
          $scope.comments.push(data);
          //$scope.commentIndex($scope.commentNew.nid);
        } 
      });
  }

  $scope.delete = function (comment) {
    $http.delete('/api/comment/'+comment.cid).
      success(function(data) {
        for(var index in $scope.comments) {
          if($scope.comments[index].cid == data) var item = index;
        }
        $scope.comments.splice(item, 1);
      });
  }

  $scope.checkPermission = function(){
    if(this.this.comment.uid == $scope.commentNew.uid) return true;
    else return false;
  };
  
});

app.controller('SettingCtrl', function ($scope, $http) {
  
});
