app.controller('ExerciseCtrl',['$scope','$routeParams', function($scope, $routeParams) {
 		$scope.answer = false;
 		$scope.showAnswer = false;
		$scope.quotes = [
			{id:10,quote:'We leiden een inschrijvingsteam op, de secretariaatsmedewerkers gaan op nascholing, we moedigen het personeel aan om hun blik te openen. Zo ontwikkel je als school een brede visie die ook gedragen wordt door het hele korps'},
			{id:2,quote:'We vragen ook aan de leerlingen om mee te komen naar het oudercontact. En dat lukt vrij aardig, tot 60-70%. Het is uiteindelijk de bedoeling om zijn schools functioneren te verbeteren, dus die is er ook best bij. Eigenlijk hoort die leerling centraal te staan in het oudercontact, dat het niet ‘over’ hem/haar gaat. Uiteindelijk moeten we in de richting gaan van het eigen leren in handen nemen, reflecteren over eigen handelen, verantwoordelijkheid nemen, …'},
			{id:4,quote:'Als we ouders niet kunnen bereiken, ook niet telefonisch, dan gaan we wel op huisbezoek. Om te weten wat er eventueel aan de hand is, waarom die leerling (soms) niet op school is. Op de manier komen we ook heel veel te weten. Meestal proberen we dat te vermijden omdat het tijdsintensief is.'},
			{id:12,quote:'Op het allereerste info-moment met de ouders geven we een lijst mee van tien punten, “hoe mijn kind thuis te ondersteunen”: voldoende en gezond eten, voldoende slapen, boekentas in orde, … Zo krijgen ze het gevoel dat ze én betrokken zijn én dat dat voor hen haalbaar is. Tegelijk zeggen we: “als er een probleem is, laat het niet groeien, bij deze personen kun je terecht'},
			{id:5,quote:'Er zijn veel die Franstalig zijn van die ouders (…) Met de verandering naar scholengroep hebben ze gezegd ‘dat mag niet, er wordt gecommuniceerd in het Nederlands, in de agenda wordt niet in een andere taal geschreven’. Ik begrijp dat, maar het wordt daarmee een gecompliceerde zaak. Wat is de bedoeling, dat ze begrijpen wat je zegt of is het de bedoeling dat het in het Nederlands staat? (…) De bedoeling is dat dat kind geholpen wordt, dan is het toch wel de bedoeling dat ze begrijpen wat ik zeg.'},
			{id:7,quote:'We gingen zoeken naar de informele contactmogelijkheden met ouders. Om te ijveren voor meer informele contacten tussen ouders en leerkrachten, hebben we wat schrik bij het personeel moeten overwinnen. Leerkrachten hebben niet altijd graag dat ouders hier zo maar rondlopen (...) Nu staan we zover dat elke klastitularis minstens tegen eind oktober iedere ouder al eens heeft gesproken. Dus nog vóór het eerste formele oudercontact. Als je al die (in)formele momenten samentelt, is de kans dus vrij groot dat we de meeste ouders na anderhalve maand al eens gezien of gesproken hebben'},
			{id:8,quote:'Scholen zelf hebben veel vragen omtrent ‘bereikbaarheid’ en ‘betrokkenheid’ van anderstalige/allochtone groepen. Maar zijn de modellen die daarvoor ontwikkeld kunnen worden niet sowieso van toepassing op alle (groepen van) ouders?'},
			{id:1,quote:'We geven hen dan het gevoel dat zij als ouders onze belangrijkste partners zijn. We benoemen het ook zo, ook als we problemen met jongeren ondervinden: jullie kennen jullie kind het beste, het langst, we hebben jullie nodig. Dat is een zeer belangrijk signaal.', visible: true},
			{id:3,quote:'Zonder samenwerking met ouders kun je de kinderen/jongeren/leerlingen niet goed begeleiden in hun leer- en leefproces. Misschien is er wel een enkeling die op eigen wilskracht bereikt wat ie wil bereiken.'},
			{id:6,quote:'De telefoon is wat mij betreft nog het makkelijkste taalmiddel. We hebben met de school ook een evolutie doorgemaakt. In het begin gingen we via het CLB, maar dat bleek omslachtig. We schakelden eerst een tolk in als er zich een zwaar probleem stelde. Dat is uiteraard geen ideaal moment. Dan hebben we een intercultureel medewerker aangeworven die nu op het secretariaat werkt en Turkse is van nationaliteit. Turks was de taal die toen het meest nodig was hier op school. Die nam dan specifieke contacten over. We hebben dus in ons personeelsbeleid ook voor wat diversiteit gezorgd. Uiteindelijk zijn we daar ook een stuk van teruggekomen. Want op de duur kreeg je het effect dat de ouders slechts met één persoon op de school contact hadden.'},
			{id:9,quote:'Bij het oudercontact moeten we er voor zorgen dat de gesprekken positief beginnen, zeker niet met wat allemaal niet goed loopt. Dat blijft een aandachtspunt.'},
			{id:11,quote:'Wij hebben een huiswerkklas voor de eerste graad. Ook voor de 2de en 3de graad een remediëringsklas waar ze terecht kunnen met vragen en moeilijkheden. Dus de school neemt die taak op zich, we verwachten dat niet van de ouders. Een school moet dus afgerond zijn als de school gedaan is. Niet dat thuis nog eens die rol moet spelen.'}
		];
		$scope.dropSuccessHandler = function($event){

		};
		$scope.onDrop = function($event, $data, accepted){
			$scope.answer = false;
			angular.forEach(accepted, function(index){
				if($data == index) $scope.answer = true;
			});
			$scope.showAnswer = true;			
		};

		$scope.toggle = function($id){
			$scope.showAnswer = false;	
			angular.forEach($scope.quotes, function(quote){
				quote.visible = false;
			});
			$scope.quotes[$id].visible = true;
		};
}]);

