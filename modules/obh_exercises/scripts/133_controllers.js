app.controller('ExerciseCtrl', function($scope, $http){
  $scope.loading = true;
  $scope.nid = null;
  $scope.uid = null;
  $scope.schoolid = null;
  $scope.settings = [];
  $scope.components = [];

  $scope.init = function(uid, nid, schoolid){
    $scope.nid = nid;
    $scope.uid = uid;
    $scope.schoolid = schoolid;
    $scope.componentIndex();
  }

  $scope.componentIndex = function(){
    $http.get('/api/webform/component/133').
      success(function(data, status, headers, config) {
        angular.forEach(data, function(component, index){
          $scope.components.push(component);
          console.log(component);

        });
	    angular.forEach($scope.components, function(component, index){ 
	    	
	        //angular.element('.cid_'+component.cid).append('<div dragdealer>dragdealer</div>');
	    });
	    $scope.loading = false;
    });
  }
});