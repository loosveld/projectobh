app.controller('ExerciseCtrl', function($scope, $http){
  $scope.loading = true;
  $scope.nid = null;
  $scope.uid = null;
  $scope.schoolid = null;
  $scope.settings = [];
  $scope.components = [];

  $scope.init = function(uid, nid, schoolid){
    $scope.nid = nid;
    $scope.uid = uid;
    $scope.schoolid = schoolid;
    $scope.componentIndex();
  }

  $scope.componentIndex = function(){
    $http.get('/api/webform/component/172').
      success(function(data, status, headers, config) {
        angular.forEach(data, function(component, index){
          $scope.components.push(component);
        });
        console.log($scope.components);
        $http.get('/api/webform/settings/172').
          success(function(data, status, headers, config) {
            $scope.settings = data;
            angular.forEach($scope.settings, function(setting, index){
              angular.forEach($scope.components, function(component, index){
                if(setting.schoolid == $scope.schoolid && setting.cid == component.cid){
                  var key = component.form_key;
                  if(setting.visible == 0) angular.element('.cid_'+component.cid).hide();
                  //angular.element('#webform-component-'+key.replace(/_/g, '-')).addClass('hidden');
                }
              });
            });
            $scope.loading = false;
        }); 
    });
  }
});

app.controller('SettingCtrl', function ($scope, $http) {
  $scope.form = {};
  $scope.loader1 = true;
  $scope.loader2 = false;
  $scope.components = [];
  $scope.schools = [];
  $scope.settings = [];
  $scope.clist = [];
  $scope.nid = null;
  $scope.uid = null;
  $scope.sid = 2;
  $scope.schoolFilter = null;

  $scope.init = function(uid, nid){
    $scope.nid = nid;
    $scope.uid = uid;

    $scope.componentIndex();
  }

  $scope.componentIndex = function(){
    $http.get('/api/webform/component/172').
      success(function(data, status, headers, config) {
        angular.forEach(data, function(component, index){
          $scope.components.push(component);
        });

        $http.get('/api/webform/settings/172').
          success(function(data, status, headers, config) {
            $scope.settings = data;
            $http.get('/api/school').
              success(function(data, status, headers, config) {
                $scope.loader1 = false;
                $scope.loader2 = true;
                angular.forEach(data, function(school, index){
                  $scope.schools.push(school);
                  angular.forEach($scope.components, function(component, index){
                      var item = {};
                      item.cid = component.cid;
                      item.name = component.name;
                      item.schoolid = school.nid;
                      item.pid = component.pid;
                      item.visible = true;
                      $scope.clist.push(item);
                  });
                });
                angular.forEach($scope.settings, function(setting, index){
                  angular.forEach($scope.clist, function(item, c_index){
                    if(setting.schoolid == item.schoolid && item.cid == setting.cid){
                      console.log(c_index);
                      if(setting.visible == 0) $scope.clist[c_index].visible = false;
                    }
                  });
                });
            });

        }); 
    });
  }

  $scope.filterSchool = function(item){
    if($scope.schoolFilter) $scope.loader2 = false;
    else $scope.loader2 = true;

    if($scope.schoolFilter){
      if(item.schoolid == $scope.schoolFilter.nid){
        return item;
      }
    };
  }

  $scope.saveComponentSetting = function(event){
    console.log(event.target.checked);
    item = {};
    item.schoolid = this.component.schoolid;
    if(event.target.checked == true) item.visible = 1;
    else item.visible = 0;
    item.cid = this.component.cid;
    $scope.post(item);
  }

  $scope.post = function (item) {

    $http.post('/api/webform/component/'+$scope.nid, item).
      success(function(data) {
        //console.log(data);
      });
  }


});



  

