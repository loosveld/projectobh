app.controller('CommentCtrl', function($scope, $http) {
    $scope.form = {};
    $scope.loading = true;
    $scope.editMode = false;
    $scope.comments = [];
    $scope.uid = 0;
    $scope.nid = 0;
    $scope.type = '';
    $scope.commentNew = {
        nid: 0,
        cid: 0,
        subject: '',
        is_new: 1,
        pid: 0,
        uid: 0,
        mail: '',
        is_anonymous: 0,
        homepage: '',
        status: 1,
        language: 'und',
        comment_body: {
            und: [{
                value: '',
                format: 'filtered_html'
            }]
        },
        field_comment_type: {
            und: [{
                value: ''
            }]
        }
    };

    $scope.init = function(nid, uid, type) {
        $scope.commentNew.nid = nid;
        $scope.commentNew.uid = uid;
        $scope.commentNew.field_comment_type.und[0].value = type;
        $scope.commentIndex(type, nid);
    }

    $scope.commentIndex = function(type, nid) {
        $http.get('/api/comment/' + type + '/' + nid).
        success(function(data, status, headers, config) {
            $scope.comments = data;
            $scope.loading = false;
            console.log(data);
        });
    }

    $scope.toggleEdit = function() {
        this.comment.editMode = !this.comment.editMode;
    }

    $scope.commentDelete = function() {
        $scope.delete(this.comment);
    }

    $scope.commentAdd = function() {
        if ($scope.formCommentNew.$valid) {
            $scope.post($scope.commentNew);
        }
    }

    $scope.commentSave = function() {
        var comment = this.this.comment;
        $scope.post(comment);
        this.comment.editMode = !this.comment.editMode;
    }

    $scope.post = function(comment) {
        $http.post('/api/comment', comment).
        success(function(data) {
            if (data.is_new) {
                $scope.comments.push(data);
                //$scope.commentIndex($scope.commentNew.nid);
            }
        });
    }

    $scope.delete = function(comment) {
        $http.delete('/api/comment/' + comment.cid).
        success(function(data) {
            for (var index in $scope.comments) {
                if ($scope.comments[index].cid == data) var item = index;
            }
            $scope.comments.splice(item, 1);
        });
    }

    $scope.checkPermission = function() {
        //return true;
        if (this.this.comment.uid == $scope.commentNew.uid) return true;
        else return false;
    };

});
