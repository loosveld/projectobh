// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var compass = require('gulp-compass');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');

var path_theme = 'themes/projectobh/';

// Lint Task
gulp.task('lint', function() {
    return gulp.src(path_theme + 'js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    gulp.src(path_theme + '/sass/*.scss')
        .pipe(plumber())
        .pipe(compass({
            css: path_theme +'css',
            sass: path_theme + 'sass',
            image: path_theme +'images',
            import_path: 'libraries',
            require: ['susy']
        }))
        .pipe(gulp.dest(path_theme + 'css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(path_theme + 'js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(path_theme + 'js/*.js', ['lint', 'scripts']);
    gulp.watch(path_theme + 'sass/*.scss', ['sass']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);